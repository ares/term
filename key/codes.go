// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package key

// Code represents the key code.
type Code []byte

// == Function keys
//

var (
	C_F1  = Code{0, ';', 59}
	C_F2  = Code{0, ';', 60}
	C_F3  = Code{0, ';', 61}
	C_F4  = Code{0, ';', 62}
	C_F5  = Code{0, ';', 63}
	C_F6  = Code{0, ';', 64}
	C_F7  = Code{0, ';', 65}
	C_F8  = Code{0, ';', 66}
	C_F9  = Code{0, ';', 67}
	C_F10 = Code{0, ';', 68}
	C_F11 = Code{0, ';', 133}
	C_F12 = Code{0, ';', 134}
)

var (
	C_SHIFT_F1  = Code{0, ';', 84}
	C_SHIFT_F2  = Code{0, ';', 85}
	C_SHIFT_F3  = Code{0, ';', 86}
	C_SHIFT_F4  = Code{0, ';', 87}
	C_SHIFT_F5  = Code{0, ';', 88}
	C_SHIFT_F6  = Code{0, ';', 89}
	C_SHIFT_F7  = Code{0, ';', 90}
	C_SHIFT_F8  = Code{0, ';', 91}
	C_SHIFT_F9  = Code{0, ';', 92}
	C_SHIFT_F10 = Code{0, ';', 93}
	C_SHIFT_F11 = Code{0, ';', 135}
	C_SHIFT_F12 = Code{0, ';', 136}
)

var (
	C_CTRL_F1  = Code{0, ';', 94}
	C_CTRL_F2  = Code{0, ';', 95}
	C_CTRL_F3  = Code{0, ';', 96}
	C_CTRL_F4  = Code{0, ';', 97}
	C_CTRL_F5  = Code{0, ';', 98}
	C_CTRL_F6  = Code{0, ';', 99}
	C_CTRL_F7  = Code{0, ';', 100}
	C_CTRL_F8  = Code{0, ';', 101}
	C_CTRL_F9  = Code{0, ';', 102}
	C_CTRL_F10 = Code{0, ';', 103}
	C_CTRL_F11 = Code{0, ';', 137}
	C_CTRL_F12 = Code{0, ';', 138}
)

var (
	C_ALT_F1  = Code{0, ';', 104}
	C_ALT_F2  = Code{0, ';', 105}
	C_ALT_F3  = Code{0, ';', 106}
	C_ALT_F4  = Code{0, ';', 107}
	C_ALT_F5  = Code{0, ';', 108}
	C_ALT_F6  = Code{0, ';', 109}
	C_ALT_F7  = Code{0, ';', 110}
	C_ALT_F8  = Code{0, ';', 111}
	C_ALT_F9  = Code{0, ';', 112}
	C_ALT_F10 = Code{0, ';', 113}
	C_ALT_F11 = Code{0, ';', 139}
	C_ALT_F12 = Code{0, ';', 140}
)

// == Cursor
//

// Numeric keypad

var (
	C_NUMPAD_HOME        = Code{0, ';', 71}
	C_NUMPAD_UP_ARROW    = Code{0, ';', 72}
	C_NUMPAD_PAGE_UP     = Code{0, ';', 73}
	C_NUMPAD_LEFT_ARROW  = Code{0, ';', 75}
	C_NUMPAD_RIGHT_ARROW = Code{0, ';', 77}
	C_NUMPAD_END         = Code{0, ';', 79}
	C_NUMPAD_DOWN_ARROW  = Code{0, ';', 80}
	C_NUMPAD_PAGE_DOWN   = Code{0, ';', 81}
	C_NUMPAD_INSERT      = Code{0, ';', 82}
	C_NUMPAD_DELETE      = Code{0, ';', 83}
)

var (
	C_NUMPAD_SHIFT_HOME        = Code{55}
	C_NUMPAD_SHIFT_UP_ARROW    = Code{56}
	C_NUMPAD_SHIFT_PAGE_UP     = Code{57}
	C_NUMPAD_SHIFT_LEFT_ARROW  = Code{52}
	C_NUMPAD_SHIFT_RIGHT_ARROW = Code{54}
	C_NUMPAD_SHIFT_END         = Code{49}
	C_NUMPAD_SHIFT_DOWN_ARROW  = Code{50}
	C_NUMPAD_SHIFT_PAGE_DOWN   = Code{51}
	C_NUMPAD_SHIFT_INSERT      = Code{48}
	C_NUMPAD_SHIFT_DELETE      = Code{46}
)

var (
	C_NUMPAD_CTRL_HOME        = Code{0, ';', 119}
	C_NUMPAD_CTRL_UP_ARROW    = Code{0, ';', 141} // Some keyboards
	C_NUMPAD_CTRL_PAGE_UP     = Code{0, ';', 132}
	C_NUMPAD_CTRL_LEFT_ARROW  = Code{0, ';', 115}
	C_NUMPAD_CTRL_RIGHT_ARROW = Code{0, ';', 116}
	C_NUMPAD_CTRL_END         = Code{0, ';', 117}
	C_NUMPAD_CTRL_DOWN_ARROW  = Code{0, ';', 145} // Some keyboards
	C_NUMPAD_CTRL_PAGE_DOWN   = Code{0, ';', 118}
	C_NUMPAD_CTRL_INSERT      = Code{0, ';', 146} // Some keyboards
	C_NUMPAD_CTRL_DELETE      = Code{0, ';', 147} // Some keyboards
)

// * * *

var (
	C_HOME        = Code{224, ';', 71} // Some keyboards
	C_UP_ARROW    = Code{224, ';', 72} // Some keyboards
	C_PAGE_UP     = Code{224, ';', 73} // Some keyboards
	C_LEFT_ARROW  = Code{224, ';', 75} // Some keyboards
	C_RIGHT_ARROW = Code{224, ';', 77} // Some keyboards
	C_END         = Code{224, ';', 79} // Some keyboards
	C_DOWN_ARROW  = Code{224, ';', 80} // Some keyboards
	C_PAGE_DOWN   = Code{224, ';', 81} // Some keyboards
	C_INSERT      = Code{224, ';', 82} // Some keyboards
	C_DELETE      = Code{224, ';', 83} // Some keyboards
)

var (
	C_SHIFT_HOME        = C_HOME        // Some keyboards
	C_SHIFT_UP_ARROW    = C_UP_ARROW    // Some keyboards
	C_SHIFT_PAGE_UP     = C_PAGE_UP     // Some keyboards
	C_SHIFT_LEFT_ARROW  = C_LEFT_ARROW  // Some keyboards
	C_SHIFT_RIGHT_ARROW = C_RIGHT_ARROW // Some keyboards
	C_SHIFT_END         = C_END         // Some keyboards
	C_SHIFT_DOWN_ARROW  = C_DOWN_ARROW  // Some keyboards
	C_SHIFT_PAGE_DOWN   = C_PAGE_DOWN   // Some keyboards
	C_SHIFT_INSERT      = C_INSERT      // Some keyboards
	C_SHIFT_DELETE      = C_DELETE      // Some keyboards
)

var (
	C_CTRL_HOME        = Code{224, ';', 119} // Some keyboards
	C_CTRL_UP_ARROW    = Code{224, ';', 141} // Some keyboards
	C_CTRL_PAGE_UP     = Code{224, ';', 132} // Some keyboards
	C_CTRL_LEFT_ARROW  = Code{224, ';', 115} // Some keyboards
	C_CTRL_RIGHT_ARROW = Code{224, ';', 116} // Some keyboards
	C_CTRL_END         = Code{224, ';', 117} // Some keyboards
	C_CTRL_DOWN_ARROW  = Code{224, ';', 145} // Some keyboards
	C_CTRL_PAGE_DOWN   = Code{224, ';', 118} // Some keyboards
	C_CTRL_INSERT      = Code{224, ';', 146} // Some keyboards
	C_CTRL_DELETE      = Code{224, ';', 147} // Some keyboards
)

var (
	C_ALT_HOME        = Code{224, ';', 151} // Some keyboards
	C_ALT_UP_ARROW    = Code{224, ';', 152} // Some keyboards
	C_ALT_PAGE_UP     = Code{224, ';', 153} // Some keyboards
	C_ALT_LEFT_ARROW  = Code{224, ';', 155} // Some keyboards
	C_ALT_RIGHT_ARROW = Code{224, ';', 157} // Some keyboards
	C_ALT_END         = Code{224, ';', 159} // Some keyboards
	C_ALT_DOWN_ARROW  = Code{224, ';', 154} // Some keyboards
	C_ALT_PAGE_DOWN   = Code{224, ';', 161} // Some keyboards
	C_ALT_INSERT      = Code{224, ';', 162} // Some keyboards
	C_ALT_DELETE      = Code{224, ';', 163} // Some keyboards
)

// == Misc

var C_CTRL_PRINT_SCREEN = Code{0, ';', 144}

var C_CTRL_PAUSE = Code{0, ';', 0}

var (
	C_BACKSPACE       = Code{8}
	C_SHIFT_BACKSPACE = Code{8}
	C_CTRL_BACKSPACE  = Code{127}
	C_ALT_BACKSPACE   = Code{0} // Some keyboards TODO: remove because is the same than down?
)

var (
	C_ENTER      = Code{13}
	C_CTRL_ENTER = Code{10}
	C_ALT_ENTER  = Code{0} // Some keyboards TODO: remove because is the same than up?
)

var (
	C_TAB       = Code{9}
	C_SHIFT_TAB = Code{0, ';', 15}
	C_CTRL_TAB  = Code{0, ';', 148} // Some keyboards
	C_ALT_TAB   = Code{0, ';', 165} // Some keyboards
)

var C_NULL = Code{0, ';', 3}

// == Letters
//

var (
	C_A = Code{97}
	C_B = Code{98}
	C_C = Code{99}
	C_D = Code{100}
	C_E = Code{101}
	C_F = Code{102}
	C_G = Code{103}
	C_H = Code{104}
	C_I = Code{105}
	C_J = Code{106}
	C_K = Code{107}
	C_L = Code{108}
	C_M = Code{109}
	C_N = Code{110}
	C_O = Code{111}
	C_P = Code{112}
	C_Q = Code{113}
	C_R = Code{114}
	C_S = Code{115}
	C_T = Code{116}
	C_U = Code{117}
	C_V = Code{118}
	C_W = Code{119}
	C_X = Code{120}
	C_Y = Code{121}
	C_Z = Code{122}
)

var (
	C_SHIFT_A = Code{65}
	C_SHIFT_B = Code{66}
	C_SHIFT_C = Code{67}
	C_SHIFT_D = Code{68}
	C_SHIFT_E = Code{69}
	C_SHIFT_F = Code{70}
	C_SHIFT_G = Code{71}
	C_SHIFT_H = Code{72}
	C_SHIFT_I = Code{73}
	C_SHIFT_J = Code{74}
	C_SHIFT_K = Code{75}
	C_SHIFT_L = Code{76}
	C_SHIFT_M = Code{77}
	C_SHIFT_N = Code{78}
	C_SHIFT_O = Code{79}
	C_SHIFT_P = Code{80}
	C_SHIFT_Q = Code{81}
	C_SHIFT_R = Code{82}
	C_SHIFT_S = Code{83}
	C_SHIFT_T = Code{84}
	C_SHIFT_U = Code{85}
	C_SHIFT_V = Code{86}
	C_SHIFT_W = Code{87}
	C_SHIFT_X = Code{88}
	C_SHIFT_Y = Code{89}
	C_SHIFT_Z = Code{90}
)

var (
	C_CTRL_A = Code{1}
	C_CTRL_B = Code{2}
	C_CTRL_C = Code{3}
	C_CTRL_D = Code{4}
	C_CTRL_E = Code{5}
	C_CTRL_F = Code{6}
	C_CTRL_G = Code{7}
	C_CTRL_H = Code{8}
	C_CTRL_I = Code{9}
	C_CTRL_J = Code{10}
	C_CTRL_K = Code{11}
	C_CTRL_L = Code{12}
	C_CTRL_M = Code{13}
	C_CTRL_N = Code{14}
	C_CTRL_O = Code{15}
	C_CTRL_P = Code{16}
	C_CTRL_Q = Code{17}
	C_CTRL_R = Code{18}
	C_CTRL_S = Code{19}
	C_CTRL_T = Code{20}
	C_CTRL_U = Code{21}
	C_CTRL_V = Code{22}
	C_CTRL_W = Code{23}
	C_CTRL_X = Code{24}
	C_CTRL_Y = Code{25}
	C_CTRL_Z = Code{26}
)

var (
	C_ALT_A = Code{0, ';', 30}
	C_ALT_B = Code{0, ';', 48}
	C_ALT_C = Code{0, ';', 46}
	C_ALT_D = Code{0, ';', 32}
	C_ALT_E = Code{0, ';', 18}
	C_ALT_F = Code{0, ';', 33}
	C_ALT_G = Code{0, ';', 34}
	C_ALT_H = Code{0, ';', 35}
	C_ALT_I = Code{0, ';', 23}
	C_ALT_J = Code{0, ';', 36}
	C_ALT_K = Code{0, ';', 37}
	C_ALT_L = Code{0, ';', 38}
	C_ALT_M = Code{0, ';', 50}
	C_ALT_N = Code{0, ';', 49}
	C_ALT_O = Code{0, ';', 24}
	C_ALT_P = Code{0, ';', 25}
	C_ALT_Q = Code{0, ';', 16}
	C_ALT_R = Code{0, ';', 19}
	C_ALT_S = Code{0, ';', 31}
	C_ALT_T = Code{0, ';', 20}
	C_ALT_U = Code{0, ';', 22}
	C_ALT_V = Code{0, ';', 47}
	C_ALT_W = Code{0, ';', 17}
	C_ALT_X = Code{0, ';', 45}
	C_ALT_Y = Code{0, ';', 21}
	C_ALT_Z = Code{0, ';', 44}
)

// == Numbers
//

var (
	C_0 = Code{48}
	C_1 = Code{49}
	C_2 = Code{50}
	C_3 = Code{51}
	C_4 = Code{52}
	C_5 = Code{53}
	C_6 = Code{54}
	C_7 = Code{55}
	C_8 = Code{56}
	C_9 = Code{57}
)

var (
	C_SHIFT_0 = Code{41}
	C_SHIFT_1 = Code{33}
	C_SHIFT_2 = Code{64}
	C_SHIFT_3 = Code{35}
	C_SHIFT_4 = Code{36}
	C_SHIFT_5 = Code{37}
	C_SHIFT_6 = Code{94}
	C_SHIFT_7 = Code{38}
	C_SHIFT_8 = Code{42}
	C_SHIFT_9 = Code{40}
)

var (
	//C_CTRL_0 = Code{}
	//C_CTRL_1 = Code{}
	C_CTRL_2 = Code{0}
	//C_CTRL_3 = Code{}
	//C_CTRL_4 = Code{}
	//C_CTRL_5 = Code{}
	C_CTRL_6 = Code{30}
	//C_CTRL_7 = Code{}
	//C_CTRL_8 = Code{}
	//C_CTRL_9 = Code{}
)

var (
	C_ALT_0 = Code{0, ';', 129}
	C_ALT_1 = Code{0, ';', 120}
	C_ALT_2 = Code{0, ';', 121}
	C_ALT_3 = Code{0, ';', 122}
	C_ALT_4 = Code{0, ';', 123}
	C_ALT_5 = Code{0, ';', 124}
	C_ALT_6 = Code{0, ';', 125}
	C_ALT_7 = Code{0, ';', 126}
	C_ALT_8 = Code{0, ';', 127}
	C_ALT_9 = Code{0, ';', 128}
)

// == Punctuation signs
//

var (
	C_HYPHEN       = Code{45} // '-'
	C_EQUAL        = Code{61} // '='
	C_LBRACKET     = Code{91} // '['
	C_RBRACKET     = Code{93} // ']'
	C_SPACE        = Code{92} // ' '
	C_SEMICOLON    = Code{59} // ';'
	C_APOSTROPHE   = Code{39} // '\''
	C_COMMA        = Code{44} // ','
	C_PERIOD       = Code{46} // '.'
	C_SLASH        = Code{47} // '/'
	C_GRAVE_ACCENT = Code{96} // '`'
)

var (
	C_SHIFT_HYPHEN       = Code{95}
	C_SHIFT_EQUAL        = Code{43}
	C_SHIFT_LBRACKET     = Code{123}
	C_SHIFT_RBRACKET     = Code{125}
	C_SHIFT_SPACE        = Code{124}
	C_SHIFT_SEMICOLON    = Code{58}
	C_SHIFT_APOSTROPHE   = Code{34}
	C_SHIFT_COMMA        = Code{60}
	C_SHIFT_PERIOD       = Code{62}
	C_SHIFT_SLASH        = Code{63}
	C_SHIFT_GRAVE_ACCENT = Code{126}
)

var (
	C_CTRL_HYPHEN = Code{31}
	//C_CTRL_EQUAL        = Code{}
	C_CTRL_LBRACKET = Code{27}
	C_CTRL_RBRACKET = Code{29}
	C_CTRL_SPACE    = Code{28}
	//C_CTRL_SEMICOLON    = Code{}
	//C_CTRL_APOSTROPHE   = Code{}
	//C_CTRL_COMMA        = Code{}
	//C_CTRL_PERIOD       = Code{}
	//C_CTRL_SLASH        = Code{}
	//C_CTRL_GRAVE_ACCENT = Code{}
)

var (
	C_ALT_HYPHEN       = Code{0, ';', 130}
	C_ALT_EQUAL        = Code{0, ';', 131}
	C_ALT_LBRACKET     = Code{0, ';', 26}
	C_ALT_RBRACKET     = Code{0, ';', 27}
	C_ALT_SPACE        = Code{0, ';', 43}
	C_ALT_SEMICOLON    = Code{0, ';', 39}
	C_ALT_APOSTROPHE   = Code{0, ';', 40}
	C_ALT_COMMA        = Code{0, ';', 51}
	C_ALT_PERIOD       = Code{0, ';', 52}
	C_ALT_SLASH        = Code{0, ';', 53}
	C_ALT_GRAVE_ACCENT = Code{0, ';', 41}
)

// == Numeric keypad misc.
//

var (
	C_NUMPAD_ENTER    = Code{13}
	C_NUMPAD_DIVIDE   = Code{47} // '/'
	C_NUMPAD_MULTIPLY = Code{42} // '*'
	C_NUMPAD_SUBTRACT = Code{45} // '-'
	C_NUMPAD_ADD      = Code{43} // '+'

	C_NUMPAD_5 = Code{0, ';', 76} // Some keyboards
)

var (
	//C_NUMPAD_SHIFT_ENTER    = Code{}
	C_NUMPAD_SHIFT_DIVIDE   = Code{47}
	C_NUMPAD_SHIFT_MULTIPLY = Code{0, ';', 144} // Some keyboards
	C_NUMPAD_SHIFT_SUBTRACT = Code{45}
	C_NUMPAD_SHIFT_ADD      = Code{43}
	C_NUMPAD_SHIFT_5        = Code{53}
)

var (
	C_NUMPAD_CTRL_ENTER    = Code{10}
	C_NUMPAD_CTRL_DIVIDE   = Code{0, ';', 142} // Some keyboards
	C_NUMPAD_CTRL_MULTIPLY = Code{0, ';', 78}  // Some keyboards
	C_NUMPAD_CTRL_SUBTRACT = Code{0, ';', 149} // Some keyboards
	C_NUMPAD_CTRL_ADD      = Code{0, ';', 150} // Some keyboards
	C_NUMPAD_CTRL_5        = Code{0, ';', 143} // Some keyboards
)

var (
	C_NUMPAD_ALT_ENTER  = Code{0, ';', 166} // Some keyboards
	C_NUMPAD_ALT_DIVIDE = Code{0, ';', 74}  // Some keyboards
	//C_NUMPAD_ALT_MULTIPLY = Code{}
	C_NUMPAD_ALT_SUBTRACT = Code{0, ';', 164} // Some keyboards
	C_NUMPAD_ALT_ADD      = Code{0, ';', 55}  // Some keyboards
	//C_NUMPAD_ALT_5        = Code{}
)
