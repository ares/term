// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Package key contains values that represent keyboard keys and key combinations.
//
//
// IMPORTANT
//
// Some of the values in the Codes tables are not valid for all computers.
// Check the computer's documentation for values that are different.
//
// Some codes are not available on some keyboards. "ANSI.SYS" will not interpret
// the codes in parentheses for those keyboards unless you specify the "/X"
// switch in the DEVICE command for "ANSI.SYS".
package key

const (
	BACKSPACE = 8
	TAB       = 9
	ENTER     = 13
	ESCAPE    = 27
)

// Control+letters keys.
const (
	CTRL_A = 1 + iota
	CTRL_B
	CTRL_C
	CTRL_D
	CTRL_E
	CTRL_F
	CTRL_G
	CTRL_H
	CTRL_I
	CTRL_J
	CTRL_K
	CTRL_L
	CTRL_M
	CTRL_N
	CTRL_O
	CTRL_P
	CTRL_Q
	CTRL_R
	CTRL_S
	CTRL_T
	CTRL_U
	CTRL_V
	CTRL_W
	CTRL_X
	CTRL_Y
	CTRL_Z
)

/* Maybe necessary to translating it to other languages.

// Letters.
const (
	A = 97 + iota
	B
	C
	D
	E
	F
	G
	H
	I
	J
	K
	L
	M
	N
	O
	P
	Q
	R
	S
	T
	U
	V
	W
	X
	Y
	Z
)

// Shift+letters keys.
const (
	SHIFT_A = 65 + iota
	SHIFT_B
	SHIFT_C
	SHIFT_D
	SHIFT_E
	SHIFT_F
	SHIFT_G
	SHIFT_H
	SHIFT_I
	SHIFT_J
	SHIFT_K
	SHIFT_L
	SHIFT_M
	SHIFT_N
	SHIFT_O
	SHIFT_P
	SHIFT_Q
	SHIFT_R
	SHIFT_S
	SHIFT_T
	SHIFT_U
	SHIFT_V
	SHIFT_W
	SHIFT_X
	SHIFT_Y
	SHIFT_Z
)

// Numeric keys.
const (
	N0 = 48 + iota
	N1
	N2
	N3
	N4
	N5
	N6
	N7
	N8
	N9
)*/
