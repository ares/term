// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package key

// winuser.h
// http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx

// Virtual-key codes
const (
	VK_CANCEL = 0x03 // Control-break processing
	VK_BACK   = 0x08 // BACKSPACE key
	VK_TAB    = 0x09 // TAB key
	VK_CLEAR  = 0x0C // CLEAR key
	VK_RETURN = 0x0D // RETURN key

	VK_SHIFT   = 0x10 // SHIFT key
	VK_CONTROL = 0x11 // CTRL key
	VK_MENU    = 0x12 // ALT key
	VK_CAPITAL = 0x14 // CAPS LOCK key
	VK_ESCAPE  = 0x1B // ESC key

	VK_SPACE  = 0x20 // SPACEBAR
	VK_PRIOR  = 0x21 // PAGE UP key
	VK_NEXT   = 0x22 // PAGE DOWN key
	VK_END    = 0x23 // END key
	VK_HOME   = 0x24 // HOME key
	VK_LEFT   = 0x25 // LEFT ARROW key
	VK_UP     = 0x26 // UP ARROW key
	VK_RIGHT  = 0x27 // RIGHT ARROW key
	VK_DOWN   = 0x28 // DOWN ARROW key
	VK_INSERT = 0x2D // INS key
	VK_DELETE = 0x2E // DEL key

	VK_LSHIFT   = 0xA0 // Left SHIFT key
	VK_RSHIFT   = 0xA1 // Right SHIFT key
	VK_LCONTROL = 0xA2 // Left CONTROL key
	VK_RCONTROL = 0xA3 // Right CONTROL key
	VK_LMENU    = 0xA4 // Left ALT key
	VK_RMENU    = 0xA5 // Right ALT key
)

// Letters key codes.
const (
	VK_A = iota + 0x41
	VK_B
	VK_C
	VK_D
	VK_E
	VK_F
	VK_G
	VK_H
	VK_I
	VK_J
	VK_K
	VK_L
	VK_M
	VK_N
	VK_O
	VK_P
	VK_Q
	VK_R
	VK_S
	VK_T
	VK_U
	VK_V
	VK_W
	VK_X
	VK_Y
	VK_Z
)
