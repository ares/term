// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Package ansi provides the ANSI Escape sequences.
//
// ANSI escape sequence is a sequence of ASCII characters, the first two of
// which are the ASCII "Escape" character 27 (1Bh) and the left-bracket character
// "[" (5Bh). The character or characters following the escape and left-bracket
// characters specify an alphanumeric code that controls a keyboard or display
// function.
//
// Most Unix terminals interpret ANSI Escape sequences to change display
// graphics, control cursor movement, and reassign keys.
//
// The references about ANSI Escape sequences have been got from
// http://ascii-table.com/ansi-escape-sequences.php and
// http://www.termsys.demon.co.uk/vtansi.htm
package ansi

import (
	"fmt"
	"strconv"

	"bitbucket.org/ares/term/key"
)

// CursorPosition returns the sequence to move the cursor to the specified
// position (coordinates).
// If you do not specify a position, the cursor moves to the home position at
// the upper-left corner of the screen (line 0, column 0). This escape sequence
// works the same way as the following Cursor Position escape sequence.
func CursorPosition(line, column byte) []byte {
	return []byte(fmt.Sprintf("\033[%s;%sH",
		strconv.Itoa(int(line)), strconv.Itoa(int(column)),
	))
}

// CursorUp returns the sequence to move the cursor up by the specified number
// of lines without changing columns.
// If the cursor is already on the top line, ANSI.SYS ignores this sequence.
func CursorUp(value byte) []byte {
	return []byte(fmt.Sprintf("\033[%sA", strconv.Itoa(int(value))))
}

// CursorDown returns the sequence to move the cursor down by the specified
// number of lines without changing columns.
// If the cursor is already on the bottom line, ANSI.SYS ignores this sequence.
func CursorDown(value byte) []byte {
	return []byte(fmt.Sprintf("\033[%sB", strconv.Itoa(int(value))))
}

// CursorForward returns the sequence to move the cursor forward by the
// specified number of columns without changing lines.
// If the cursor is already in the rightmost column, ANSI.SYS ignores this sequence.
func CursorForward(value byte) []byte {
	return []byte(fmt.Sprintf("\033[%sC", strconv.Itoa(int(value))))
}

// CursorBackward returns the sequence to move the cursor back by the specified
// number of columns without changing lines.
// If the cursor is already in the leftmost column, ANSI.SYS ignores this sequence.
func CursorBackward(value byte) []byte {
	return []byte(fmt.Sprintf("\033[%sD", strconv.Itoa(int(value))))
}

// SaveCursorPosition returns the sequence to save the current cursor position.
// You can move the cursor to the saved cursor position by using the
// Restore Cursor Position sequence.
func SaveCursorPosition() []byte { return []byte("\033[s") }

// RestoreCursorPosition returns the sequence to return the cursor to the
// position stored by the Save Cursor Position sequence.
func RestoreCursorPosition() []byte { return []byte("\033[u") }

// EraseDisplay returns the sequence to clear the screen.
func EraseDisplay() []byte { return []byte("\033[2J") }

// EraseLine returns the sequence to clear all characters from the cursor
// position to the end of the line (including the character at the cursor position).
func EraseLine() []byte { return []byte("\033[K") }

// * * *

// TextAttr represents the text attribute in graphics mode.
type TextAttr int

// Text attributes
const (
	Off        TextAttr = iota // All attributes off
	Bold                       // Bold on
	Underscore                 // Underscore (on monochrome display adapter only)
	Blink                      // Blink on
	Reverse                    // Reverse video on
	Concealed                  // Concealed on
)

// Color represents the color in graphics mode.
type Color int

// Colors
const (
	Black Color = iota
	Red
	Green
	Yellow
	Blue
	Magenta
	Cyan
	White
)

// BackColor represents the background color.
type BackColor int

// ForeColor represents the foreground color.
type ForeColor int

// Background returns the ANSI code of color for the background.
func Background(c Color) BackColor { return BackColor(c + 40) }

// Foreground returns the ANSI code of color for the foreground.
func Foreground(c Color) ForeColor { return ForeColor(c + 30) }

// SetGraphicsMode returns the sequence to call the graphics functions specified
// by the following values. These specified functions remain active until the
// next occurrence of this escape sequence.
// Graphics mode changes the colors and attributes of text (such as bold and
// underline) displayed on the screen.
func SetGraphicsMode(back BackColor, fore ForeColor, attr TextAttr) []byte {
	added := false
	seq := "\033["

	if back > -1 {
		seq += strconv.Itoa(int(back))
		added = true
	}
	if fore > -1 {
		if added {
			seq += ";"
		}
		seq += strconv.Itoa(int(fore))
		added = true
	}
	if attr > -1 {
		if added {
			seq += ";"
		}
		seq += strconv.Itoa(int(attr))
		added = true
	}

	if added {
		seq += "m"
		return []byte(seq)
	}
	return nil
}

// The next ANSI sequences seem do not work, at least in Linux.
// * * *

// ScreenMode represents the screen mode.
type ScreenMode byte

// Screen mode
const (
	T40x25x2     ScreenMode = iota // 40 x 25 monochrome (text)
	T40x25                         // 40 x 25 color (text)
	T80x25x2                       // 80 x 25 monochrome (text)
	T80x25                         // 80 x 25 color (text)
	G320x200x4                     // 320 x 200 4-color (graphics)
	G320x200x2                     // 320 x 200 monochrome (graphics)
	G640x200x2                     // 640 x 200 monochrome (graphics)
	LineWrapping                   // Enables line wrapping

	G320x200     = 13 + iota // 320 x 200 color (graphics)
	G640x200x16              // 640 x 200 color (16-color graphics)
	G640x350x2               // 640 x 350 monochrome (2-color graphics)
	G640x350x16              // 640 x 350 color (16-color graphics)
	G640x480x2               // 640 x 480 monochrome (2-color graphics)
	G640x480x16              // 640 x 480 color (16-color graphics)
	G320x200x256             // 320 x 200 color (256-color graphics)
)

// SetMode returns the sequence to change the screen width or type to the mode
// specified by one of the values in ScreenMode.
func SetMode(mod ScreenMode) []byte {
	return []byte(fmt.Sprintf("\033[=%sh", strconv.Itoa(int(mod))))
}

// ResetMode returns the sequence to reset the mode by using the same values
// that SetMode uses, except for LineWrapping, which disables line wrapping.
func ResetMode(mod ScreenMode) []byte {
	return []byte(fmt.Sprintf("\033[=%sl", strconv.Itoa(int(mod))))
}

// SetKeyboardStrings returns the sequence to redefine a keyboard key to a
// specified code.
//
// oldCode is one or more of the values listed in the Code tables in package "key".
// newCode is either the ASCII code for a single character or a string contained
// in quotation marks. For example, both 65 and "A" can be used to represent an
// uppercase A.
func SetKeyboardStrings(oldCode key.Code, newCode string) []byte {
	seq := "\033["
	for _, v := range oldCode {
		seq += strconv.Itoa(int(v))
	}
	seq += ";" + newCode + "p"

	return []byte(seq)
}
