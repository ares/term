// +build ignore

#include <windows.h>
#include <stdio.h>

HANDLE hStdin;
DWORD fdwSaveOldMode;

VOID ErrorExit(LPSTR);
VOID ResizeEventProc(WINDOW_BUFFER_SIZE_RECORD);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
   DWORD cNumRead, fdwMode, i;
    INPUT_RECORD irInBuf[128];
    int counter=0;

    hStdin = GetStdHandle(STD_INPUT_HANDLE);
    if (hStdin == INVALID_HANDLE_VALUE)
        ErrorExit("GetStdHandle");

    if (! GetConsoleMode(hStdin, &fdwSaveOldMode) )
        ErrorExit("GetConsoleMode");

    fdwMode = ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT;
    if (! SetConsoleMode(hStdin, fdwMode) )
        ErrorExit("SetConsoleMode");

    while (counter++ <= 500) {
        if (! ReadConsoleInput(hStdin, irInBuf, 128, &cNumRead))
            ErrorExit("ReadConsoleInput");

        for (i = 0; i < cNumRead; i++) {
            switch(irInBuf[i].EventType) {
                case WINDOW_BUFFER_SIZE_EVENT:
                    ResizeEventProc(irInBuf[i].Event.WindowBufferSizeEvent);
                    break;
            }
        }
    }

    SetConsoleMode(hStdin, fdwSaveOldMode);
    return 0;
}

VOID ResizeEventProc(WINDOW_BUFFER_SIZE_RECORD wbsr) {
    printf("Resize event\n");
    printf("%d columns X %d rows.\n", wbsr.dwSize.X, wbsr.dwSize.Y);
}

VOID ErrorExit (LPSTR lpszMessage) {
    fprintf(stderr, "%s\n", lpszMessage);
    SetConsoleMode(hStdin, fdwSaveOldMode);
    ExitProcess(0);
}
