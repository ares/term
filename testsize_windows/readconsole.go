// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// +build windows

package main

import (
	"fmt"
	"os"

	"bitbucket.org/ares/term"
)

func ReadConsole() error {
	ter, err := term.New()
	if err != nil {
		return err
	}
	defer ter.Restore()

	// Enable the window and mouse input events.
	var state uint32 = 0
	state &^= ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT
	if err = ter.SetMode(state); err != nil {
		return err
	}

	var input _INPUT_RECORD
	var numEvents uint32
	if err := _ReadConsoleInput(term.InputFD, &input, 0, &numEvents); err != nil {
		return os.NewSyscallError("ReadConsoleInput", err)
	}

	//fmt.Println("Press Enter to exit")

	//L:
	for {
		_ReadConsoleInput(term.InputFD, &input, 64, &numEvents)
		numEvents_ := int(numEvents)

		for i := 0; i < numEvents_; i++ {

			if input.EventType == WINDOW_BUFFER_SIZE_EVENT {
				fmt.Printf("Size: %d X %d\n", input.WindowBufferSizeEvent.dwSize.x,
					input.WindowBufferSizeEvent.dwSize.y)
			} /*else if input.EventType == KEY_EVENT && input.KeyEvent.bKeyDown &&
				input.KeyEvent.wVirtualKeyCode == VK_RETURN {
				break L
			}*/
			//fmt.Printf("Event type: %d", input.EventType)
		}
	}
	return nil
}
