// +build ignore

#define WINVER 0x0501
#include <windows.h>
#include <iostream>

using namespace std;

LRESULT APIENTRY MainWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_SIZE:
        cout<<"Window re-sized"<<endl;
        break;
        case WM_DESTROY:
        PostQuitMessage(0);
        break;
        default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    HWND hwnd;
    MSG Msg;
    const char lpcszClassName[] = "messageClass";
    WNDCLASSEX  WindowClassEx;

    ZeroMemory(&WindowClassEx, sizeof(WNDCLASSEX));
    WindowClassEx.cbSize        = sizeof(WNDCLASSEX);
    WindowClassEx.lpfnWndProc   = MainWndProc;
    WindowClassEx.hInstance     = hInstance;
    WindowClassEx.lpszClassName = lpcszClassName;

    if (RegisterClassEx(&WindowClassEx) != 0)
    {
        // Create a message-only window
    hwnd = CreateWindowEx(0, lpcszClassName, NULL, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, hInstance, NULL);
        if (hwnd != NULL)
        cout<<"Window created"<<endl;
        else
        UnregisterClass(lpcszClassName, hInstance);
    }
    ShowWindow(hwnd,nCmdShow);
    while(GetMessage(&Msg, NULL, 0, 0) > 0)
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }
     return (int)Msg.wParam;
}
