// go.mksyscall -w -lazy=false def_windows.go
// MACHINE GENERATED BY go.mksyscall (github.com/kless/gotool/go.mksyscall); DO NOT EDIT

package main

import (
	"syscall"
	"unsafe"
)

var (
	modkernel32 = syscall.MustLoadDLL("kernel32.dll")
	moduser32   = syscall.MustLoadDLL("user32.dll")

	procGetConsoleWindow  = modkernel32.MustFindProc("GetConsoleWindow")
	procReadConsoleInputW = modkernel32.MustFindProc("ReadConsoleInputW")
	procGetModuleHandleW  = modkernel32.MustFindProc("GetModuleHandleW")
	procCallWindowProcW   = moduser32.MustFindProc("CallWindowProcW")
	procCreateWindowExW   = moduser32.MustFindProc("CreateWindowExW")
	procDefWindowProcW    = moduser32.MustFindProc("DefWindowProcW")
	procDispatchMessageW  = moduser32.MustFindProc("DispatchMessageW")
	procGetMessageW       = moduser32.MustFindProc("GetMessageW")
	procRegisterClassExW  = moduser32.MustFindProc("RegisterClassExW")
	procShowWindow        = moduser32.MustFindProc("ShowWindow")
	procTranslateMessage  = moduser32.MustFindProc("TranslateMessage")
	procUpdateWindow      = moduser32.MustFindProc("UpdateWindow")
	procSetWindowLongW    = moduser32.MustFindProc("SetWindowLongW")
)

func _GetConsoleWindow() (hWnd syscall.Handle) {
	r0, _, _ := syscall.Syscall(procGetConsoleWindow.Addr(), 0, 0, 0, 0)
	hWnd = syscall.Handle(r0)
	return
}

func _ReadConsoleInput(hConsoleInput syscall.Handle, lpBuffer *_INPUT_RECORD, nLength uint32, lpNumberOfEventsRead *uint32) (err error) {
	r1, _, e1 := syscall.Syscall6(procReadConsoleInputW.Addr(), 4, uintptr(hConsoleInput), uintptr(unsafe.Pointer(lpBuffer)), uintptr(nLength), uintptr(unsafe.Pointer(lpNumberOfEventsRead)), 0, 0)
	if int(r1) == 0 {
		if e1 != 0 {
			err = error(e1)
		} else {
			err = syscall.EINVAL
		}
	}
	return
}

func _GetModuleHandle(lpModuleName *uint16) (handle syscall.Handle, err error) {
	r0, _, e1 := syscall.Syscall(procGetModuleHandleW.Addr(), 1, uintptr(unsafe.Pointer(lpModuleName)), 0, 0)
	handle = syscall.Handle(r0)
	if handle == 0 {
		if e1 != 0 {
			err = error(e1)
		} else {
			err = syscall.EINVAL
		}
	}
	return
}

func _CallWindowProc(lpPrevWndFunc uintptr, hWnd syscall.Handle, Msg uint32, wParam uintptr, lParam uintptr) (ret uintptr) {
	r0, _, _ := syscall.Syscall6(procCallWindowProcW.Addr(), 5, uintptr(lpPrevWndFunc), uintptr(hWnd), uintptr(Msg), uintptr(wParam), uintptr(lParam), 0)
	ret = uintptr(r0)
	return
}

func _CreateWindowEx(dwExStyle uint32, lpClassName *uint16, lpWindowName *uint16, dwStyle uint32, x int32, y int32, nWidth int32, nHeight int32, hWndParent syscall.Handle, hMenu syscall.Handle, hInstance syscall.Handle, lpParam uintptr) (hWnd syscall.Handle, err error) {
	r0, _, e1 := syscall.Syscall12(procCreateWindowExW.Addr(), 12, uintptr(dwExStyle), uintptr(unsafe.Pointer(lpClassName)), uintptr(unsafe.Pointer(lpWindowName)), uintptr(dwStyle), uintptr(x), uintptr(y), uintptr(nWidth), uintptr(nHeight), uintptr(hWndParent), uintptr(hMenu), uintptr(hInstance), uintptr(lpParam))
	hWnd = syscall.Handle(r0)
	if hWnd == 0 {
		if e1 != 0 {
			err = error(e1)
		} else {
			err = syscall.EINVAL
		}
	}
	return
}

func _DefWindowProc(hWnd syscall.Handle, Msg uint32, wParam uintptr, lParam uintptr) (ret uintptr) {
	r0, _, _ := syscall.Syscall6(procDefWindowProcW.Addr(), 4, uintptr(hWnd), uintptr(Msg), uintptr(wParam), uintptr(lParam), 0, 0)
	ret = uintptr(r0)
	return
}

func _DispatchMessage(lpMsg *_MSG) (ret uintptr) {
	r0, _, _ := syscall.Syscall(procDispatchMessageW.Addr(), 1, uintptr(unsafe.Pointer(lpMsg)), 0, 0)
	ret = uintptr(r0)
	return
}

func _GetMessage(lpMsg *_MSG, hWnd syscall.Handle, wMsgFilterMin uint32, wMsgFilterMax uint32) (ret bool, err error) {
	r0, _, e1 := syscall.Syscall6(procGetMessageW.Addr(), 4, uintptr(unsafe.Pointer(lpMsg)), uintptr(hWnd), uintptr(wMsgFilterMin), uintptr(wMsgFilterMax), 0, 0)
	ret = bool(r0 != 0)
	if !ret {
		if e1 != 0 {
			err = error(e1)
		} else {
			err = syscall.EINVAL
		}
	}
	return
}

func _RegisterClassEx(lpwcx *_WNDCLASSEX) (atom uint16, err error) {
	r0, _, e1 := syscall.Syscall(procRegisterClassExW.Addr(), 1, uintptr(unsafe.Pointer(lpwcx)), 0, 0)
	atom = uint16(r0)
	if atom == 0 {
		if e1 != 0 {
			err = error(e1)
		} else {
			err = syscall.EINVAL
		}
	}
	return
}

func _ShowWindow(hWnd syscall.Handle, nCmdShow int32) (ok bool) {
	r0, _, _ := syscall.Syscall(procShowWindow.Addr(), 2, uintptr(hWnd), uintptr(nCmdShow), 0)
	ok = bool(r0 != 0)
	return
}

func _TranslateMessage(lpMsg *_MSG) (ret bool) {
	r0, _, _ := syscall.Syscall(procTranslateMessage.Addr(), 1, uintptr(unsafe.Pointer(lpMsg)), 0, 0)
	ret = bool(r0 != 0)
	return
}

func _UpdateWindow(hWnd syscall.Handle) (ok bool) {
	r0, _, _ := syscall.Syscall(procUpdateWindow.Addr(), 1, uintptr(hWnd), 0, 0)
	ok = bool(r0 != 0)
	return
}

func _SetWindowLongPtr(hWnd syscall.Handle, nIndex int32, dwNewLong int32) (ret int32, err error) {
	r0, _, e1 := syscall.Syscall(procSetWindowLongW.Addr(), 3, uintptr(hWnd), uintptr(nIndex), uintptr(dwNewLong))
	ret = int32(r0)
	if ret == 0 {
		if e1 != 0 {
			err = error(e1)
		} else {
			err = syscall.EINVAL
		}
	}
	return
}
