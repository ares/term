// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// +build windows

package main

import (
	"fmt"
	"os"
	"syscall"
	"unsafe"
)

// === NEW ===
var oldWindowProc int32 //uintptr in amd64

// Just like Message1 but using SetWindowLongPtr.
func Message2() error {
	consoleInstance, err := _GetModuleHandle(nil)
	if err != nil {
		return os.NewSyscallError("GetModuleHandle", err)
	}

	// Create callback
	winProc := syscall.NewCallback(windowProc2)

	// === NEW ===
	// Get the window handle used by the console associated with the calling process.
	consoleWinHandle := _GetConsoleWindow()

	oldWindowProc, err = _SetWindowLongPtr(consoleWinHandle, _GWLP_WNDPROC, int32(winProc))
	if err != nil {
		return os.NewSyscallError("SetWindowLongPtr", err)
	}
	// ___ NEW ___

	// Register the window class

	className, err := syscall.UTF16PtrFromString("consoleClass")
	if err != nil {
		return err
	}

	consoleClass := _WNDCLASSEX{
		size:      uint32(unsafe.Sizeof(_WNDCLASSEX{})),
		wndProc:   winProc,
		instance:  consoleInstance,
		className: className,
	}
	if _, err := _RegisterClassEx(&consoleClass); err != nil {
		return os.NewSyscallError("RegisterClassEx", err)
	}

	// Create the message-only window

	msgWinHandle, err := _CreateWindowEx(
		0,
		className, nil,
		0,
		0, 0, 0, 0,
		_HWND_MESSAGE, 0, consoleInstance, 0,
	)
	if err != nil {
		return os.NewSyscallError("CreateWindowEx", err)
	}
	fmt.Printf("main window handle is %x\n", msgWinHandle)

	_ShowWindow(msgWinHandle, 3)

	// Dispatch message

	var ok bool
	var msg _MSG
	//for ok, err = _GetMessage(&msg, 0, 0, 0); ok;; {
	for {
		ok, err = _GetMessage(&msg, 0, 0, 0)
		if err != nil {
			return os.NewSyscallError("GetMessage", err)
		}
		if !ok {
			println("break GetMessage")
			break
		}

		_TranslateMessage(&msg) // Does some additional processing on keyboard events.
		_DispatchMessage(&msg)
	}
	return nil
}

func windowProc2(hwnd syscall.Handle, msg uint32, wparam, lparam uintptr) (ret uintptr) {
	switch msg {
	case _WM_EXITSIZEMOVE:
		// send channel
		println("message: EXITSIZEMOVE")
	case _WM_SIZE:
		if wparam == _SIZE_MAXIMIZED || wparam == _SIZE_RESTORED {
			// send channel
			println("message: SIZE")
		}
	default:
		//println("default: windowProc")
		// === NEW ===
		return _CallWindowProc(uintptr(oldWindowProc), hwnd, msg, wparam, lparam)
	}
	return 0
}
