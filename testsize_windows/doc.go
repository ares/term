/*
Windows

This program runs some tests built to try to know when the size of the console
window has been changed; also, there is code in C++ used to test it directly,
avoiding some possible error during the wrapping in Go.

It is not possible to know when the window size has been re-sized:

http://www.cplusplus.com/forum/windows/102499/
http://stackoverflow.com/questions/16628460/how-to-handle-the-message-only-window-to-get-the-messages-from-the-console-windo
http://social.msdn.microsoft.com/Forums/en-US/windowsgeneraldevelopmentissues/thread/250925d6-b742-4c66-a47c-7e82c0a9d7f5

This is the great problem of to use a closed and propietary API, where the
developers can not read the source code.

The problem is due to that cannot be retrieved "WM_SIZE" / "WM_EXITSIZEMOVE" / other
messages for windows that exist outside of your process (the command prompt is
not part of your process), UAC stops you from doing this.

You can instead start an goroutine comparing the values every second/few hundred
milliseconds to check weather or not the window size has changed, and consider
that to be an "WM_SIZE" event instead.

	https://groups.google.com/forum/#!topic/golang-nuts/XWJf-o4dsXc


Move window

When you start dragging a window, the system enters a modal move/resize loop; it
does not return to your own message loop until the drag action has finished. You
are still getting "WM_SIZE" because it is sent directly to the window procedure,
but it does not flow through your own message loop.

At the beginning of such a modal drag action, the system sends "WM_ENTERSIZEMOVE"
to your window procedure. When you release the mouse button, your application
will get "WM_EXITSIZEMOVE". That is probably the message you want to trigger on.


Information

To look for information about a function in the Windows API, I use a searcher with:

	site:microsoft.com windows library [Name]

This is an example to create a window:

	http://www.winprog.org/tutorial/simple_window.html
*/
package main
