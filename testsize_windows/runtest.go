// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// +build windows

package main

import (
	"flag"
	"fmt"
	"log"
	"time"
)

var Time = flag.Uint("t", 10, "time in seconds to wait in every test")
var duration time.Duration
var fail = make(chan bool)

func main() {
	flag.Parse()
	duration = time.Duration(*Time) * time.Second

	log.SetFlags(0)
	log.SetPrefix("--- FAIL: ")

	fmt.Printf("Every test has been a different attempt to get the message "+
		"related to the change of window size into a console.\nIt takes %d seconds "+
		"in every one, if there is not a failure.\n", *Time)

	TestMessage1()
	TestMessage2()
	TestReadConsole()
}

func TestMessage1() {
	fmt.Print("\n=== RUN TestMessage1\n")

	go func() {
		if err := Message1(); err != nil {
			log.Print(err)
			fail <- true
		}
	}()
	select {
	case <-fail:
	case <-time.After(duration):
	}
}

func TestMessage2() {
	fmt.Print("\n=== RUN TestMessage2\n")

	go func() {
		if err := Message2(); err != nil {
			log.Print(err)
			fail <- true
		}
	}()
	select {
	case <-fail:
	case <-time.After(duration):
	}
}

func TestReadConsole() {
	fmt.Print("\n=== RUN TestReadConsole\n")

	go func() {
		if err := ReadConsole(); err != nil {
			log.Print(err)
			fail <- true
		}
	}()
	select {
	case <-fail:
	case <-time.After(duration):
	}
}
