// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import "syscall"

// wincon.h

/*
#define KEY_EVENT 1
#define MOUSE_EVENT 2
#define WINDOW_BUFFER_SIZE_EVENT 4
#define MENU_EVENT 8
#define FOCUS_EVENT 16
#define CAPSLOCK_ON 128
#define ENHANCED_KEY 256
#define RIGHT_ALT_PRESSED 1
#define LEFT_ALT_PRESSED 2
#define RIGHT_CTRL_PRESSED 4
#define LEFT_CTRL_PRESSED 8
#define SHIFT_PRESSED 16
#define NUMLOCK_ON 32
#define SCROLLLOCK_ON 64
#define FROM_LEFT_1ST_BUTTON_PRESSED 1
#define RIGHTMOST_BUTTON_PRESSED 2
#define FROM_LEFT_2ND_BUTTON_PRESSED 4
#define FROM_LEFT_3RD_BUTTON_PRESSED 8
#define FROM_LEFT_4TH_BUTTON_PRESSED 16
#define MOUSE_MOVED	1
#define DOUBLE_CLICK	2
#define MOUSE_WHEELED	4
*/

const (
	ENABLE_PROCESSED_INPUT = 1
	ENABLE_LINE_INPUT      = 2
	ENABLE_ECHO_INPUT      = 4
	ENABLE_WINDOW_INPUT    = 8
	ENABLE_MOUSE_INPUT     = 16
	ENABLE_INSERT_MODE     = 32
	ENABLE_QUICK_EDIT_MODE = 64
	ENABLE_EXTENDED_FLAGS  = 128
	ENABLE_AUTO_POSITION   = 256

	ENABLE_PROCESSED_OUTPUT   = 1
	ENABLE_WRAP_AT_EOL_OUTPUT = 2
)

const (
	KEY_EVENT                = 1
	MOUSE_EVENT              = 2
	WINDOW_BUFFER_SIZE_EVENT = 4
	MENU_EVENT               = 8
	FOCUS_EVENT              = 16
	CAPSLOCK_ON              = 128
	ENHANCED_KEY             = 256

	RIGHT_CTRL_PRESSED = 4
	LEFT_CTRL_PRESSED  = 8
)

// http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx
// Virtual-key codes
const (
	VK_BACK   = 0x08 // BACKSPACE
	VK_RETURN = 0x0D
	VK_C      = 0x43
)

/*
typedef struct _CONSOLE_SCREEN_BUFFER_INFO {
	COORD	dwSize;
	COORD	dwCursorPosition;
	WORD	wAttributes;
	SMALL_RECT srWindow;
	COORD	dwMaximumWindowSize;
} CONSOLE_SCREEN_BUFFER_INFO,*PCONSOLE_SCREEN_BUFFER_INFO;

typedef struct _COORD {
	SHORT X;
	SHORT Y;
} COORD, *PCOORD;

typedef struct _SMALL_RECT {
	SHORT Left;
	SHORT Top;
	SHORT Right;
	SHORT Bottom;
} SMALL_RECT, *PSMALL_RECT;
*/

type _CONSOLE_SCREEN_BUFFER_INFO struct {
	dwSize              _COORD
	dwCursorPosition    _COORD
	wAttributes         uint16
	srWindow            _SMALL_RECT
	dwMaximumWindowSize _COORD
}

type _COORD struct {
	x, y int16
}

type _SMALL_RECT struct {
	left, top, right, bottom int16
}

/*
typedef struct _INPUT_RECORD {
	WORD EventType;
	union {
		KEY_EVENT_RECORD KeyEvent;
		MOUSE_EVENT_RECORD MouseEvent;
		WINDOW_BUFFER_SIZE_RECORD WindowBufferSizeEvent;
		MENU_EVENT_RECORD MenuEvent;
		FOCUS_EVENT_RECORD FocusEvent;
	} Event;
} INPUT_RECORD,*PINPUT_RECORD;

typedef struct _KEY_EVENT_RECORD {
	BOOL bKeyDown;
	WORD wRepeatCount;
	WORD wVirtualKeyCode;
	WORD wVirtualScanCode;
	union {
		WCHAR UnicodeChar;
		CHAR AsciiChar;
	} uChar;
	DWORD dwControlKeyState;
}
#ifdef __GNUC__
// gcc's alignment is not what win32 expects
PACKED
#endif
KEY_EVENT_RECORD;

typedef struct _MOUSE_EVENT_RECORD {
	COORD dwMousePosition;
	DWORD dwButtonState;
	DWORD dwControlKeyState;
	DWORD dwEventFlags;
} MOUSE_EVENT_RECORD;

typedef struct _WINDOW_BUFFER_SIZE_RECORD {	COORD dwSize; } WINDOW_BUFFER_SIZE_RECORD;

typedef struct _MENU_EVENT_RECORD {	UINT dwCommandId; } MENU_EVENT_RECORD,*PMENU_EVENT_RECORD;

typedef struct _FOCUS_EVENT_RECORD {	BOOL bSetFocus; } FOCUS_EVENT_RECORD;
*/

type _INPUT_RECORD struct {
	EventType uint16
	_Event
}

type _Event struct {
	KeyEvent              _KEY_EVENT_RECORD
	MouseEvent            _MOUSE_EVENT_RECORD
	WindowBufferSizeEvent _WINDOW_BUFFER_SIZE_RECORD
	MenuEvent             _MENU_EVENT_RECORD
	FocusEvent            _FOCUS_EVENT_RECORD
}

type _KEY_EVENT_RECORD struct {
	bKeyDown          bool
	_                 [3]byte
	wRepeatCount      uint16
	wVirtualKeyCode   uint16
	wVirtualScanCode  uint16
	uChar             uint16 // UnicodeChar
	dwControlKeyState uint32
}

type _MOUSE_EVENT_RECORD struct {
	dwMousePosition   _COORD
	dwButtonState     uint32
	dwControlKeyState uint32
	dwEventFlags      uint32
}

type _WINDOW_BUFFER_SIZE_RECORD struct {
	dwSize _COORD
}

type _MENU_EVENT_RECORD struct {
	dwCommandId uint32
}

type _FOCUS_EVENT_RECORD struct {
	bSetFocus bool
	_         [3]byte
}

// winuser.h

const _GWLP_WNDPROC = -4

const (
	_WM_SIZE = 5

	_SIZE_RESTORED  = 0
	_SIZE_MINIMIZED = 1
	_SIZE_MAXIMIZED = 2
	_SIZE_MAXSHOW   = 3
	_SIZE_MAXHIDE   = 4
)

const _WM_EXITSIZEMOVE = 0x0232

/*
typedef struct tagMSG {
	HWND   hwnd;
	UINT   message;
	WPARAM wParam;
	LPARAM lParam;
	DWORD  time;
	POINT  pt;
} MSG, *PMSG, *LPMSG;

typedef struct tagPOINT {
	LONG x;
	LONG y;
} POINT, *PPOINT;
*/

type _MSG struct {
	hwnd    syscall.Handle
	message uint32
	wParam  uintptr
	lParam  uintptr
	time    uint32
	pt      _POINT
}

type _POINT struct {
	x, y int32
}

/*
typedef struct _WNDCLASSEXW {
	UINT cbSize;
	UINT style;
	WNDPROC lpfnWndProc;
	int cbClsExtra;
	int cbWndExtra;
	HINSTANCE hInstance;
	HICON hIcon;
	HCURSOR hCursor;
	HBRUSH hbrBackground;
	LPCWSTR lpszMenuName;
	LPCWSTR lpszClassName;
	HICON hIconSm;
} WNDCLASSEXW,*LPWNDCLASSEXW,*PWNDCLASSEXW;
*/

type _WNDCLASSEX struct {
	size       uint32
	style      uint32
	wndProc    uintptr
	clsExtra   int32
	wndExtra   int32
	instance   syscall.Handle
	icon       syscall.Handle
	cursor     syscall.Handle
	background syscall.Handle
	menuName   *uint16
	className  *uint16
	iconSm     syscall.Handle
}

// winbase.h

// WaitForSingleObject
const (
	_WAIT_ABANDONED = 0x00000080
	_WAIT_OBJECT_0  = 0
	_WAIT_TIMEOUT   = 0x00000102
	_WAIT_FAILED    = 0xFFFFFFFF
	_INFINITE       = 0xFFFFFFFF
)

// #define HWND_MESSAGE ((HWND)(-3))
const _HWND_MESSAGE syscall.Handle = -3 & (1<<32 - 1)

//const _HWND_MESSAGE syscall.Handle = ^0 - (-3) + 1

// API

// +sys _GetConsoleWindow() (hWnd HWND) = GetConsoleWindow
// +sys _ReadConsoleInput(hConsoleInput HANDLE, lpBuffer _PINPUT_RECORD, nLength DWORD, lpNumberOfEventsRead LPDWORD) (err error) = ReadConsoleInputW

// +sys _GetModuleHandle(lpModuleName LPCTSTR) (handle HMODULE, err error) = GetModuleHandleW
//+sys _WaitForSingleObject(hHandle HANDLE, dwMilliseconds DWORD) (ret DWORD) = WaitForSingleObject

// +sys _CallWindowProc(lpPrevWndFunc WNDPROC, hWnd HWND, Msg UINT, wParam WPARAM, lParam LPARAM) (ret LRESULT) = user32.CallWindowProcW
// +sys _CreateWindowEx(dwExStyle DWORD, lpClassName LPCTSTR, lpWindowName LPCTSTR, dwStyle DWORD, x int, y int, nWidth int, nHeight int, hWndParent HWND, hMenu HMENU, hInstance HINSTANCE, lpParam LPVOID) (hWnd HWND, err error)= user32.CreateWindowExW
// +sys _DefWindowProc(hWnd HWND, Msg UINT, wParam WPARAM, lParam LPARAM) (ret LRESULT) = user32.DefWindowProcW

// +sys _DispatchMessage(lpMsg *_MSG) (ret LRESULT) = user32.DispatchMessageW
//+sys _GetKeyState(nVirtKey int) (status SHORT) = user32.GetKeyState
// +sys _GetMessage(lpMsg _LPMSG, hWnd HWND, wMsgFilterMin UINT, wMsgFilterMax UINT) (ret BOOL, err error) [failretval==-1] = user32.GetMessageW
//+sys _MoveWindow(hWnd HWND, X int, Y int, nWidth int, nHeight int, bRepaint BOOL) (ok BOOL, err error) = user32.MoveWindow
// +sys _RegisterClassEx(lpwcx *_WNDCLASSEX) (atom ATOM, err error) = user32.RegisterClassExW
// +sys _ShowWindow(hWnd HWND, nCmdShow int) (ok BOOL) = user32.ShowWindow
// +sys _TranslateMessage(lpMsg *_MSG) (ret BOOL) = user32.TranslateMessage
// +sys _UpdateWindow(hWnd HWND) (ok BOOL) = user32.UpdateWindow

//32 bits:
// +sys _SetWindowLongPtr(hWnd HWND, nIndex int, dwNewLong LONG) (ret LONG, err error) = user32.SetWindowLongW
// 64 bits:
//sys _SetWindowLongPtr(hWnd HWND, nIndex int, dwNewLong LONG_PTR) (ret LONG_PTR, err error) = user32.SetWindowLongPtrW
