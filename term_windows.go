// Copyright 2012 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// To show UniCode characters in the Windows console, it needs to have an
// Unicode font such as the Lucida TTF --Consolas is built in Windows 7--.
//
// Run:
//
//   reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Console\TrueTypeFont" /v 0 /d Lucida Console
//   reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Console\TrueTypeFont" /v 00 /d Consolas
//
// Now, those fonts will be an option in the “Command Prompt” Properties; set one of them.
package term

import (
	"fmt"
	"io"
	"os"
	"syscall"

	"bitbucket.org/ares/term/sys"
)

// == Code page
//

// http://msdn.microsoft.com/en-us/library/windows/desktop/dd317756%28v=vs.85%29.aspx

// Unicode code page identifiers
const (
	//CP_UTF7 uint32 = 65000 // Unicode (UTF-7)
	CP_UTF8 uint32 = 65001 // Unicode (UTF-8)

	// UTF-16 is not possible to be set in Windows XP.
	CP_UTF16    uint32 = 1200 // Unicode UTF-16, little endian byte order (BMP of ISO 10646)
	CP_UTF16_BE uint32 = 1201 // Unicode UTF-16, big endian byte order
)

// Code page to set in the term.
var CodePage = CP_UTF8

// == Console
//

// Default values for input and output.
var (
	InputFD syscall.Handle = syscall.Stdin
	Input   io.Reader      = os.Stdin
	Output  io.Writer      = os.Stdout
)

type Terminal struct {
	inputFD, outputFD syscall.Handle

	mode modeType

	// Contain the state of a terminal, allowing to restore the original settings
	oldInputState, lastInputState uint32
	//oldOutputState, lastOutputState uint32

	// Original code page
	oldCP, oldOutputCP uint32

	// Window size
	consoleHandle syscall.Handle
	size          sys.RECT
}

// New creates a new terminal interface in the file descriptor InputFD.
func New() (*Terminal, error) {
	var t Terminal

	// Get the actual state
	err := sys.GetConsoleMode(InputFD, &t.lastInputState)
	if err != nil {
		return nil, os.NewSyscallError("sys.GetConsoleMode", err)
	}

	// Set code page to Unicode.
	cp := sys.GetConsoleCP()
	if cp != CodePage {
		if err = sys.SetConsoleCP(CodePage); err != nil {
			return nil, os.NewSyscallError("sys.SetConsoleCP", err)
		}
		t.oldCP = cp
	}
	cp = sys.GetConsoleOutputCP()
	if cp != CodePage {
		if err = sys.SetConsoleOutputCP(CodePage); err != nil {
			return nil, os.NewSyscallError("sys.SetConsoleOutputCP", err)
		}
		t.oldOutputCP = cp
	}

	// Get the window handle used by the console associated with the calling process.
	t.consoleHandle = sys.GetConsoleWindow()

	t.oldInputState = t.lastInputState // the actual state is copied to another one
	t.inputFD = InputFD
	//t.oldOutputState = t.lastOutputState
	//t.outputFD = syscall.Stdout

	return &t, nil
}

// == Restore

type State struct {
	wrap uint32
}

// OriginalState returns the terminal's original state.
func (t *Terminal) OriginalState() State {
	return State{t.oldInputState}
}

// Restore restores the original settings for the term.
func (t *Terminal) Restore() error {
	// Restore code page.
	if t.oldCP != 0 {
		sys.SetConsoleCP(t.oldCP)
	}
	if t.oldOutputCP != 0 {
		sys.SetConsoleOutputCP(t.oldOutputCP)
	}

	if t.mode != 0 {
		if err := sys.SetConsoleMode(t.inputFD, t.oldInputState); err != nil {
			return os.NewSyscallError("sys.SetConsoleMode", err)
		}
		t.lastInputState = t.oldInputState
		t.mode = 0
	}
	return nil
}

// Restore restores the settings from State.
func Restore(handle syscall.Handle, st State) error {
	if err := sys.SetConsoleMode(handle, st.wrap); err != nil {
		return os.NewSyscallError("sys.SetConsoleMode", err)
	}
	return nil
}

// == Modes

// RawMode sets the terminal to something like the "raw" mode. Input is available
// character by character, echoing is disabled, and all special processing of
// terminal input and output characters is disabled.
//
// NOTE: in tty "raw mode", CR+LF is used for output and CR is used for input.
func (t *Terminal) RawMode() error {
	t.lastInputState = 0
	t.lastInputState &^= (sys.ENABLE_LINE_INPUT | sys.ENABLE_PROCESSED_INPUT |
		sys.ENABLE_ECHO_INPUT | sys.ENABLE_WINDOW_INPUT)

	// in Stdout
	//t.lastInputState &^= (sys.ENABLE_PROCESSED_OUTPUT | sys.ENABLE_WRAP_AT_EOL_OUTPUT)

	// Put the terminal in raw mode
	if err := sys.SetConsoleMode(t.inputFD, t.lastInputState); err != nil {
		return os.NewSyscallError("sys.SetConsoleMode", err)
	}
	t.mode |= RawMode
	return nil
}

// EchoMode turns the echo mode.
func (t *Terminal) EchoMode(echo bool) error {
	if echo {
		t.lastInputState |= sys.ENABLE_ECHO_INPUT
	} else {
		t.lastInputState &^= sys.ENABLE_ECHO_INPUT
	}

	if err := sys.SetConsoleMode(t.inputFD, t.lastInputState); err != nil {
		return os.NewSyscallError("sys.SetConsoleMode", err)
	}

	if echo {
		t.mode |= EchoMode
	} else {
		t.mode &^= EchoMode
	}
	return nil
}

// CharMode sets the terminal to single-character mode.
func (t *Terminal) CharMode() (err error) {
	t.lastInputState = 0

	if err = sys.SetConsoleMode(t.inputFD, t.lastInputState); err != nil {
		return os.NewSyscallError("sys.SetConsoleMode", err)
	}
	t.mode |= CharMode

	go func() {
		var input sys.INPUT_RECORD
		var numEvents uint32

		// The possible error is checked once.
		if err = sys.ReadConsoleInput(t.inputFD, &input, 0, &numEvents); err != nil {
			fmt.Fprint(os.Stderr, os.NewSyscallError("sys.ReadConsoleInput", err))
			return
		}

		for {
			sys.ReadConsoleInput(t.inputFD, &input, 1, &numEvents)
			if t.mode&CharMode == 0 {
				break
			}
		}
	}()
	return nil
}

// SetMode sets the terminal attributes given by state.
// Warning: The use of this function is not cross-system.
func (t *Terminal) SetMode(state uint32) error {
	if err := sys.SetConsoleMode(t.inputFD, state); err != nil {
		return os.NewSyscallError("sys.SetConsoleMode", err)
	}

	t.lastInputState = state
	t.mode |= OtherMode
	return nil
}

// == Utility
//

// Fd returns the file descriptor referencing the term.
func (t *Terminal) Fd() syscall.Handle {
	return t.inputFD
}

/*func (t *Terminal) GetName() (name string, err error) {
	var title string
	if _, e := sys.GetConsoleTitle(&title, 128); e != nil {
		return "", os.NewSyscallError("sys.GetConsoleTitle", e)
	}
	return title, nil
}*/

// GetSize returns the size of the term.
func (t *Terminal) GetSize() (row, column int, err error) {
	if err = sys.GetWindowRect(t.consoleHandle, &t.size); err != nil {
		return 0, 0, os.NewSyscallError("sys.GetWindowRect", err)
	}
	return int(t.size.Right - t.size.Left), int(t.size.Bottom - t.size.Top), nil
}
