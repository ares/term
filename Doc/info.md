## Gap buffer

I had thought of using a gap buffer to handle the editing in command line.

http://www.finseth.com/craft/#c6.4.3
http://www.lazyhacker.com/slickedit/

But if you are not using a gap buffer for large chunks of text don't bother.
Shuffling around ~200 characters of a longer-than-usual command line would have
is sufficiently fast.

http://www.reddit.com/r/AskComputerScience/comments/1fphli/gap_buffer/

