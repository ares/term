// Copyright 2010 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package term

import (
	"bytes"
	"math/rand"
	"os"
	"path/filepath"
	"syscall"
	"time"
	"unicode"
	"unicode/utf16"
	"unicode/utf8"

	"bitbucket.org/ares/term/sys"
)

var shellsWithoutANSI = []string{"cmd.exe", "command.com"}

// SupportANSI checks if the terminal supports ANSI escape sequences.
func SupportANSI() bool {
	term := os.Getenv("ComSpec") // full path to the command processor
	if term == "" {
		return false
	}
	term = filepath.Base(term)

	for _, v := range shellsWithoutANSI {
		if v == term {
			return false
		}
	}
	return true
}

// Default values
const (
	d_ROW    = 24
	d_COLUMN = 80
)

// GetName gets the name of a term.
//func GetName(fd int) (string, error) {
//}

// IsTerminal returns true if the handler is a term.
func IsTerminal(handle syscall.Handle) bool {
	var st uint32
	return sys.GetConsoleMode(handle, &st) == nil
}

// ReadPassword reads characters from the input until press Enter or until
// fill in the given slice.
//
// Only reads characters that include letters, marks, numbers, punctuation,
// and symbols from Unicode categories L, M, N, P, S, besides of the
// ASCII space character.
// Ctrl-C interrumpts, and backspace removes the last character read.
//
// Returns the number of bytes read.
func ReadPassword(password []byte) (n int, err error) {
	ter, err := New()
	if err != nil {
		return 0, err
	}
	defer ter.Restore()

	// Enable the window and mouse input events.
	ter.lastInputState = 0
	ter.lastInputState &^= sys.ENABLE_WINDOW_INPUT | sys.ENABLE_MOUSE_INPUT

	if err = sys.SetConsoleMode(ter.inputFD, ter.lastInputState); err != nil {
		return 0, os.NewSyscallError("sys.SetConsoleMode", err)
	}
	ter.mode = PasswordMode

	if err = sys.FlushConsoleInputBuffer(ter.inputFD); err != nil {
		return 0, os.NewSyscallError("sys.FlushConsoleInputBuffer", err)
	}

	var input sys.INPUT_RECORD
	var numEvents uint32
	// The possible error at reading is checked once.
	if err = sys.ReadConsoleInput(ter.inputFD, &input, 0, &numEvents); err != nil {
		return 0, os.NewSyscallError("sys.ReadConsoleInput", err)
	}

	char := make([]byte, 4) // In-memory representation of a rune.
	lenPassword := 0       // Number of characters read.

	if PasswordShadowed {
		rand.Seed(int64(time.Now().Nanosecond()))
	}

L:
	for {
		sys.ReadConsoleInput(ter.inputFD, &input, 1, &numEvents)

		// Process a key down input event.
		if input.EventType != sys.KEY_EVENT || !input.KeyEvent.KeyDown {
			continue
		}

		switch input.KeyEvent.VirtualKeyCode {
		case sys.VK_RETURN:
			break L
		case sys.VK_BACK:
			if lenPassword != 0 {
				lenPassword--
				password[lenPassword] = 0
			}
			continue
		case sys.VK_C:
			if input.KeyEvent.ControlKeyState&(sys.LEFT_CTRL_PRESSED|sys.RIGHT_CTRL_PRESSED) != 0 {
				syscall.Write(syscall.Stdout, _CTRL_C)
				// Clean data stored, if any.
				for i, v := range password {
					if v == 0 {
						break
					}
					password[i] = 0
				}
				return 0, nil
			}
		}

		// Windows stores internally Unicode characters in UTF-16.
		utfChar := utf16.Decode([]uint16{input.KeyEvent.Char})[0]
		if utfChar == 0 { // For single keys such as CTRL, SHIFT, ALT.
			continue
		}

		if unicode.IsPrint(utfChar) {
			utf8.EncodeRune(char, utfChar)
			password[lenPassword] = char[0] // Only want a character by key
			lenPassword++

			if PasswordShadowed {
				syscall.Write(syscall.Stdout, bytes.Repeat(_SHADOW_CHAR, rand.Intn(3)+1))
			}
			if lenPassword == len(password) {
				break
			}
		}
	}

	syscall.Write(syscall.Stdout, _RETURN)
	n = lenPassword
	return
}

// WinSize represents a channel, Change, to know when the window size has
// changed through function DetectWinSize.
type WinSize struct {
	Change chan bool
	quit   chan bool
	wait   chan bool
}

// DetectWinSize detects whenever the window size changes, being indicated in
// channel `WinSize.Change`.
func DetectWinSize() *WinSize {
	w := &WinSize{
		make(chan bool),
		make(chan bool),
		make(chan bool),
	}

	go func() {
		consoleWinHandle := sys.GetConsoleWindow()

		var firstRect, lastRect sys.RECT
		err := sys.GetWindowRect(consoleWinHandle, &firstRect)
		if err != nil {
			panic(err)
		}

		firstX := firstRect.Right - firstRect.Left
		firstY := firstRect.Bottom - firstRect.Top
		tick := time.NewTicker(time.Second)

		for {
			select {
			case <-tick.C:
				sys.GetWindowRect(consoleWinHandle, &lastRect)
				// Window minimized
				if lastRect.Left < 0 && lastRect.Right < 0 &&
					lastRect.Top < 0 && lastRect.Bottom < 0 {
					break
				}

				lastX := lastRect.Right - lastRect.Left
				lastY := lastRect.Bottom - lastRect.Top

				if firstX != lastX || firstY != lastY {
					w.Change <- true
					firstX, firstY = lastX, lastY
				}
			case <-w.quit:
				w.wait <- true
				return
			}
		}
	}()
	return w
}

// Close closes the goroutine started to trap the signal.
func (w *WinSize) Close() {
	w.quit <- true
	<-w.wait
}
