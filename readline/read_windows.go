// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package readline

import (
//	"bufio"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/ares/term"
	"bitbucket.org/ares/term/sys"
)

// NewLine returns a line using both prompts ps1 and ps2, and setting the given
// terminal to raw mode, if were necessary.
// lenAnsi is the length of ANSI codes that the prompt ps1 could have.
// If the history is nil then it is not used.
func NewLine(ter *term.Terminal, ps1, ps2 string, lenAnsi int, hist *history) (*Line, error) {
	if ter.Mode()&term.RawMode == 0 { // the raw mode is not set
		if err := ter.RawMode(); err != nil {
			return nil, err
		}
	}

	lenPS1 := len(ps1) - lenAnsi
	_, col, err := ter.GetSize()
	if err != nil {
		return nil, err
	}

	buf := newBuffer(lenPS1, col)
	buf.insertRunes([]rune(ps1))

	return &Line{
		hasHistory(hist),
		lenPS1,
		ps1,
		ps2,
		buf,
		hist,
		ter,
	}, nil
}

// Prompt prints the primary prompt.
func (ln *Line) Prompt() (err error) {
	/*if _, err = term.Output.Write(DelLine_CR); err != nil {
		return outputError(err.Error())
	}*/
	if _, err = fmt.Fprint(term.Output, ln.ps1); err != nil {
		return outputError(err.Error())
	}

	ln.buf.pos, ln.buf.size = ln.lenPS1, ln.lenPS1
	return
}

// Read reads charactes from input to write them to output, enabling line editing.
// The errors that could return are to indicate if Ctrl+D was pressed, and for
// both input/output errors.
func (ln *Line) Read() (line string, err error) {
	var action keyAction
	var anotherLine []rune // For lines got from history.
	var isHistoryUsed bool // If the history has been accessed.

	inputFD := ln.ter.Fd()
//	in := bufio.NewReader(term.Input) // Read input.

	// Print the primary prompt.
	if err = ln.Prompt(); err != nil {
		return "", err
	}

	// == Detect change of window size.
	winSize := term.DetectWinSize()

	go func() {
		for {
			select {
			case <-winSize.Change: // Wait for.
				_, col, err := ln.ter.GetSize()
				if err != nil {
					ln.buf.columns = col
					ln.buf.refresh()
				}
			}
		}
	}()
	defer winSize.Close()

	var input sys.INPUT_RECORD
	var numEvents uint32
	if err = sys.ReadConsoleInput(inputFD, &input, 0, &numEvents); err != nil {
		return "", os.NewSyscallError("sys.ReadConsoleInput", err)
	}

	for ;; action = 0 {
		sys.ReadConsoleInput(inputFD, &input, 1, &numEvents)

		// Process a key down input event.
		if input.EventType != sys.KEY_EVENT || !input.KeyEvent.KeyDown {
			continue
		}

		switch input.KeyEvent.VirtualKeyCode {
		default:
			

		case sys.VK_RETURN:
			line = ln.buf.toString()

			if ln.useHistory {
				ln.hist.Add(line)
			}
			if _, err = term.Output.Write(CRLF); err != nil {
				return "", outputError(err.Error())
			}
			return strings.TrimSpace(line), nil

		case sys.VK_TAB:
			// TODO: disabled by now
			continue

		case sys.VK_BACK/*, sys.VK_H*/:
			if err = ln.buf.deleteCharPrev(); err != nil {
				return "", err
			}
			continue

		case sys.VK_UP:
			if !ln.useHistory {
				continue
			}
			action = _UP
		case sys.VK_DOWN:
			if !ln.useHistory {
				continue
			}
			action = _DOWN
		case sys.VK_LEFT:
			action = _LEFT
		case sys.VK_RIGHT:
			action = _RIGHT

		case sys.VK_HOME:
			action = _HOME
		case sys.VK_END:
			action = _END
		}

		// CTRL is pressed.
		if action == 0 && input.KeyEvent.ControlKeyState&(sys.LEFT_CTRL_PRESSED|sys.RIGHT_CTRL_PRESSED) != 0 {
			switch input.KeyEvent.VirtualKeyCode {
			case sys.VK_C:
				if err = ln.buf.insertRunes(ctrlC); err != nil {
					return "", err
				}
				if _, err = term.Output.Write(CRLF); err != nil {
					return "", outputError(err.Error())
				}

				//ChanCtrlC <- 1 TODO: check it

				if err = ln.Prompt(); err != nil {
					return "", err
				}
				continue
			case sys.VK_D:
				if err = ln.buf.insertRunes(ctrlD); err != nil {
					return "", err
				}
				if _, err = term.Output.Write(CRLF); err != nil {
					return "", outputError(err.Error())
				}

				ln.Restore()
				return "", ErrCtrlD

			case sys.VK_T: // Swap actual character by the previous one.
				if err = ln.buf.swap(); err != nil {
					return "", err
				}
				continue

			case sys.VK_L: // Clear screen.
				if _, err = term.Output.Write(delScreenToUpper); err != nil {
					return "", err
				}
				if err = ln.Prompt(); err != nil {
					return "", err
				}
				continue
			case sys.VK_U: // Delete the whole line.
				if err = ln.buf.deleteLine(); err != nil {
					return "", err
				}
				if err = ln.Prompt(); err != nil {
					return "", err
				}
				continue
			case sys.VK_K: // Delete from current to end of line.
				if err = ln.buf.deleteToRight(); err != nil {
					return "", err
				}
				continue

			case sys.VK_P: // Up
				if !ln.useHistory {
					continue
				}
				action = _UP
			case sys.VK_N: // Down
				if !ln.useHistory {
					continue
				}
				action = _DOWN
			case sys.VK_B: // Left
				action = _LEFT
			case sys.VK_F: // Right
				action = _RIGHT

			case sys.VK_A: // Start of line.
				action = _HOME
			case sys.VK_E: // End of line.
				action = _END
			}
		}

		switch action {
		case _UP, _DOWN: // Up and down arrow: history
			if action == _UP {
				anotherLine, err = ln.hist.Prev()
			} else {
				anotherLine, err = ln.hist.Next()
			}
			if err != nil {
				continue
			}

			// Update the current history entry before to overwrite it with
			// the next one.
			// TODO: it has to be removed before of to be saved the history
			if !isHistoryUsed {
				ln.hist.Add(ln.buf.toString())
			}
			isHistoryUsed = true

			ln.buf.grow(len(anotherLine))
			ln.buf.size = len(anotherLine) + ln.buf.promptLen
			copy(ln.buf.data[ln.lenPS1:], anotherLine)

			if err = ln.buf.refresh(); err != nil {
				return "", err
			}
			continue
		case _LEFT:
			if _, err = ln.buf.backward(); err != nil {
				return "", err
			}
			continue
		case _RIGHT:
			if _, err = ln.buf.forward(); err != nil {
				return "", err
			}
			continue
		case _HOME:
			if err = ln.buf.start(); err != nil {
				return "", err
			}
			continue
		case _END:
			if _, err = ln.buf.end(); err != nil {
				return "", err
			}
			continue
		}


/*
		char, _, err := in.ReadRune()
		if err != nil {
			return "", inputError(err.Error())
		}

		switch char {
		default:
			println(char, " ")
			if err = ln.buf.insertRune(char); err != nil {
				return "", err
			}
			continue
		}
*/

	}
}
