// Copyright 2012 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// The Windows headers can be got from directory "MinGW/include"

package sys

import "syscall"

// wincon.h

/*
#define KEY_EVENT 1
#define MOUSE_EVENT 2
#define WINDOW_BUFFER_SIZE_EVENT 4
#define MENU_EVENT 8
#define FOCUS_EVENT 16
#define CAPSLOCK_ON 128
#define ENHANCED_KEY 256
#define RIGHT_ALT_PRESSED 1
#define LEFT_ALT_PRESSED 2
#define RIGHT_CTRL_PRESSED 4
#define LEFT_CTRL_PRESSED 8
#define SHIFT_PRESSED 16
#define NUMLOCK_ON 32
#define SCROLLLOCK_ON 64
#define FROM_LEFT_1ST_BUTTON_PRESSED 1
#define RIGHTMOST_BUTTON_PRESSED 2
#define FROM_LEFT_2ND_BUTTON_PRESSED 4
#define FROM_LEFT_3RD_BUTTON_PRESSED 8
#define FROM_LEFT_4TH_BUTTON_PRESSED 16
#define MOUSE_MOVED	1
#define DOUBLE_CLICK	2
#define MOUSE_WHEELED	4
*/

const (
	ENABLE_PROCESSED_INPUT = 1
	ENABLE_LINE_INPUT      = 2
	ENABLE_ECHO_INPUT      = 4
	ENABLE_WINDOW_INPUT    = 8
	ENABLE_MOUSE_INPUT     = 16
	ENABLE_INSERT_MODE     = 32
	ENABLE_QUICK_EDIT_MODE = 64
	ENABLE_EXTENDED_FLAGS  = 128
	ENABLE_AUTO_POSITION   = 256

	ENABLE_PROCESSED_OUTPUT   = 1
	ENABLE_WRAP_AT_EOL_OUTPUT = 2
)

const (
	KEY_EVENT                = 1
	MOUSE_EVENT              = 2
	WINDOW_BUFFER_SIZE_EVENT = 4
	MENU_EVENT               = 8
	FOCUS_EVENT              = 16
	CAPSLOCK_ON              = 128
	ENHANCED_KEY             = 256
)

const (
	RIGHT_ALT_PRESSED  = 1
	LEFT_ALT_PRESSED   = 2
	RIGHT_CTRL_PRESSED = 4
	LEFT_CTRL_PRESSED  = 8
	SHIFT_PRESSED      = 16
	NUMLOCK_ON         = 32
	SCROLLLOCK_ON      = 64
)

// * * *

// The system calls only can get pointers so the arguments that use structs have
// to be converted to `uint32`.
// Note: Windows is little-endian.

type COORD struct {
	X, Y int16
}

type COORD_u uint32

func (c COORD) ToUint() COORD_u {
	return COORD_u(uint16(c.Y))<<16 | COORD_u(uint16(c.X))
}

func (c COORD_u) ToCoord() COORD {
	return COORD{int16(c), int16(c >> 16)}
}

// * * *

/*
typedef struct _CONSOLE_SCREEN_BUFFER_INFO {
	COORD	dwSize;
	COORD	dwCursorPosition;
	WORD	wAttributes;
	SMALL_RECT srWindow;
	COORD	dwMaximumWindowSize;
} CONSOLE_SCREEN_BUFFER_INFO,*PCONSOLE_SCREEN_BUFFER_INFO;

typedef struct _COORD {
	SHORT X;
	SHORT Y;
} COORD, *PCOORD;

typedef struct _SMALL_RECT {
	SHORT Left;
	SHORT Top;
	SHORT Right;
	SHORT Bottom;
} SMALL_RECT, *PSMALL_RECT;
*/

type CONSOLE_SCREEN_BUFFER_INFO struct {
	Size              COORD
	CursorPosition    COORD
	Attributes        uint16
	Window            SMALL_RECT
	MaximumWindowSize COORD
}

type SMALL_RECT struct {
	Left, Top, Right, Bottom int16
}

/*
typedef struct _INPUT_RECORD {
	WORD EventType;
	union {
		KEY_EVENT_RECORD KeyEvent;
		MOUSE_EVENT_RECORD MouseEvent;
		WINDOW_BUFFER_SIZE_RECORD WindowBufferSizeEvent;
		MENU_EVENT_RECORD MenuEvent;
		FOCUS_EVENT_RECORD FocusEvent;
	} Event;
} INPUT_RECORD,*PINPUT_RECORD;

typedef struct _KEY_EVENT_RECORD {
	BOOL bKeyDown;
	WORD wRepeatCount;
	WORD wVirtualKeyCode;
	WORD wVirtualScanCode;
	union {
		WCHAR UnicodeChar;
		CHAR AsciiChar;
	} uChar;
	DWORD dwControlKeyState;
}
#ifdef __GNUC__
// gcc's alignment is not what win32 expects
PACKED
#endif
KEY_EVENT_RECORD;

typedef struct _MOUSE_EVENT_RECORD {
	COORD dwMousePosition;
	DWORD dwButtonState;
	DWORD dwControlKeyState;
	DWORD dwEventFlags;
} MOUSE_EVENT_RECORD;

typedef struct _WINDOW_BUFFER_SIZE_RECORD {	COORD dwSize; } WINDOW_BUFFER_SIZE_RECORD;

typedef struct _MENU_EVENT_RECORD {	UINT dwCommandId; } MENU_EVENT_RECORD,*PMENU_EVENT_RECORD;

typedef struct _FOCUS_EVENT_RECORD {	BOOL bSetFocus; } FOCUS_EVENT_RECORD;

typedef struct _CHAR_INFO {
	union {
		WCHAR UnicodeChar;
		CHAR  AsciiChar;
	} Char;
	WORD  Attributes;
} CHAR_INFO, *PCHAR_INFO;
*/

type INPUT_RECORD struct {
	EventType uint16
	Event
}

type Event struct {
	KeyEvent              KEY_EVENT_RECORD
	MouseEvent            MOUSE_EVENT_RECORD
	WindowBufferSizeEvent WINDOW_BUFFER_SIZE_RECORD
	MenuEvent             MENU_EVENT_RECORD
	FocusEvent            FOCUS_EVENT_RECORD
}

type KEY_EVENT_RECORD struct {
	KeyDown         bool
	_               [3]byte
	RepeatCount     uint16
	VirtualKeyCode  uint16
	VirtualScanCode uint16
	Char            uint16 // UnicodeChar
	ControlKeyState uint32
}

type MOUSE_EVENT_RECORD struct {
	MousePosition   COORD
	ButtonState     uint32
	ControlKeyState uint32
	EventFlags      uint32
}

type WINDOW_BUFFER_SIZE_RECORD struct { Size COORD }

type MENU_EVENT_RECORD struct { CommandId uint32 }

type FOCUS_EVENT_RECORD struct {
	SetFocus bool
	_        [3]byte
}

type CHAR_INFO struct {
	Char       uint16 // UnicodeChar
	Attributes uint16
}

// The character attributes for CHAR_INFO structure.
// This member can be zero or any combination of the following values.
const (
	FOREGROUND_BLUE      = 0x0001 // Text color contains blue.
	FOREGROUND_GREEN     = 0x0002 // Text color contains green.
	FOREGROUND_RED       = 0x0004 // Text color contains red.
	FOREGROUND_INTENSITY = 0x0008 // Text color is intensified.
	BACKGROUND_BLUE      = 0x0010 // Background color contains blue.
	BACKGROUND_GREEN     = 0x0020 // Background color contains green.
	BACKGROUND_RED       = 0x0040 // Background color contains red.
	BACKGROUND_INTENSITY = 0x0080 // Background color is intensified.

	COMMON_LVB_LEADING_BYTE    = 0x0100 // Leading byte.
	COMMON_LVB_TRAILING_BYTE   = 0x0200 // Trailing byte.
	COMMON_LVB_GRID_HORIZONTAL = 0x0400 // Top horizontal
	COMMON_LVB_GRID_LVERTICAL  = 0x0800 // Left vertical.
	COMMON_LVB_GRID_RVERTICAL  = 0x1000 // Right vertical.
	COMMON_LVB_REVERSE_VIDEO   = 0x4000 // Reverse foreground and background attribute.
	COMMON_LVB_UNDERSCORE      = 0x8000 // Underscore.
)

// winuser.h

const GWLP_WNDPROC = -4

const (
	WM_SIZE = 5

	SIZE_RESTORED  = 0
	SIZE_MINIMIZED = 1
	SIZE_MAXIMIZED = 2
	SIZE_MAXSHOW   = 3
	SIZE_MAXHIDE   = 4
)

const WM_EXITSIZEMOVE = 0x0232

/*
typedef struct tagMSG {
	HWND   hwnd;
	UINT   message;
	WPARAM wParam;
	LPARAM lParam;
	DWORD  time;
	POINT  pt;
} MSG, *PMSG, *LPMSG;

typedef struct tagPOINT {
	LONG x;
	LONG y;
} POINT, *PPOINT;
*/

type MSG struct {
	hwnd    syscall.Handle
	message uint32
	wParam  uintptr
	lParam  uintptr
	time    uint32
	pt      POINT
}

type POINT struct {
	x, y int32
}

/*
typedef struct _WNDCLASSEXW {
	UINT cbSize;
	UINT style;
	WNDPROC lpfnWndProc;
	int cbClsExtra;
	int cbWndExtra;
	HINSTANCE hInstance;
	HICON hIcon;
	HCURSOR hCursor;
	HBRUSH hbrBackground;
	LPCWSTR lpszMenuName;
	LPCWSTR lpszClassName;
	HICON hIconSm;
} WNDCLASSEXW,*LPWNDCLASSEXW,*PWNDCLASSEXW;
*/

type WNDCLASSEX struct {
	size       uint32
	style      uint32
	wndProc    uintptr
	clsExtra   int32
	wndExtra   int32
	instance   syscall.Handle
	icon       syscall.Handle
	cursor     syscall.Handle
	background syscall.Handle
	menuName   *uint16
	className  *uint16
	iconSm     syscall.Handle
}

// winbase.h

// WaitForSingleObject
const (
	WAIT_ABANDONED = 0x00000080
	WAIT_OBJECT_0  = 0
	WAIT_TIMEOUT   = 0x00000102
	WAIT_FAILED    = 0xFFFFFFFF
	INFINITE       = 0xFFFFFFFF
)

// windef.h

/*
typedef struct _RECT {
	LONG left;
	LONG top;
	LONG right;
	LONG bottom;
} RECT, *PRECT;
*/

type RECT struct {
	Left   int32
	Top    int32
	Right  int32
	Bottom int32
}

// API

//+sys AllocConsole() (err error)
// +sys FillConsoleOutputAttribute(hConsoleOutput HANDLE, wAttribute WORD, nLength DWORD, dwWriteCoord _COORD_u, lpNumberOfAttrsWritten LPDWORD) (err error)
// +sys FillConsoleOutputCharacter(hConsoleOutput HANDLE, cCharacter TCHAR, nLength DWORD, dwWriteCoord _COORD_u, lpNumberOfCharsWritten LPDWORD) (err error) = FillConsoleOutputCharacterW
// +sys FlushConsoleInputBuffer(hConsoleInput HANDLE) (err error)
//+sys FreeConsole() (err error)
// +sys GetConsoleCP() (ret UINT)
// +sys GetConsoleMode(hConsoleHandle HANDLE, lpMode LPDWORD) (err error)
// +sys GetConsoleOutputCP() (ret UINT)
// +sys GetConsoleScreenBufferInfo(hConsoleOutput HANDLE, lpConsoleScreenBufferInfo _PCONSOLE_SCREEN_BUFFER_INFO) (err error)
// +sys GetConsoleTitle(lpConsoleTitle LPTSTR, nSize DWORD) (ret DWORD, err error) = GetConsoleTitleW
// +sys GetConsoleWindow() (hWnd HWND)
// +sys ReadConsoleInput(hConsoleInput HANDLE, lpBuffer _PINPUT_RECORD, nLength DWORD, lpNumberOfEventsRead LPDWORD) (err error) = ReadConsoleInputW
// +sys ScrollConsoleScreenBufferW(hConsoleOutput HANDLE, lpScrollRectangle *_SMALL_RECT, lpClipRectangle *_SMALL_RECT, dwDestinationOrigin _COORD_u, lpFill *_CHAR_INFO) (err error)
// +sys SetConsoleCP(wCodePageID UINT) (err error)
// +sys SetConsoleCursorPosition(hConsoleOutput HANDLE, dwCursorPosition _COORD_u) (err error)
// +sys SetConsoleMode(hConsoleHandle HANDLE, dwMode DWORD) (err error)
// +sys SetConsoleOutputCP(wCodePageID UINT) (err error)

// +sys GetWindowRect(hWnd HWND, lpRect _LPRECT) (err error) = user32.GetWindowRect
