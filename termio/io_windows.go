// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package termio

import (
	_"fmt"
	"os"
	"syscall"

	"bitbucket.org/ares/term/sys"
)

// Buffer represents the terminal screen buffer.
type Buffer struct {
	info       sys.CONSOLE_SCREEN_BUFFER_INFO
	startCoord sys.COORD_u // Upper left corner coordinates
	n    uint32 // number of characters written.
	size uint32

	charClear *sys.CHAR_INFO // Character for clear text.

	pos int
}

func NewBuffer() (*Buffer, error) {
	var info sys.CONSOLE_SCREEN_BUFFER_INFO

	// Get the number of character cells in the current buffer.
	err := sys.GetConsoleScreenBufferInfo(syscall.Stdout, &info)
	if err != nil {
		return nil, os.NewSyscallError("sys.GetConsoleScreenBufferInfo", err)
	}

	var startCoord sys.COORD
	return &Buffer{
		info: info,
		startCoord: startCoord.ToUint(),
		size: uint32(info.Size.X * info.Size.Y),
		charClear: &sys.CHAR_INFO{' ', 0},
	}, nil
}

// ClearScreen clears the screen.
func (buf *Buffer) ClearScreen() error {
	var charsWritten uint32

	// Fill the entire screen with blanks.
	err := sys.FillConsoleOutputCharacter(syscall.Stdout, ' ', buf.size,
		buf.startCoord, &charsWritten)
	if err != nil {
		return os.NewSyscallError("sys.FillConsoleOutputCharacter", err)
	}

	/*// Get the current text attribute.
	sys.GetConsoleScreenBufferInfo(syscall.Stdout, &info)

	// Set the buffer's attributes accordingly.
	err = sys.FillConsoleOutputAttribute(syscall.Stdout, info.Attributes, sizeConsole,
		startCoord_u, &charsWritten)
	if err != nil {
		return os.NewSyscallError("sys.FillConsoleOutputAttribute", err)
	}*/

	// Put the cursor at its home coordinates.
	if err = sys.SetConsoleCursorPosition(syscall.Stdout, buf.startCoord); err != nil {
		return os.NewSyscallError("sys.SetConsoleCursorPosition", err)
	}
	return nil
}

// ClearEOL clears until end of line, without moving cursor.
func (buf *Buffer) ClearEOL() error {
	var charsWritten uint32
	bufSizeFromCursor := uint32(buf.info.CursorPosition.X * buf.info.CursorPosition.Y)

	err := sys.FillConsoleOutputCharacter(syscall.Stdout, ' ', buf.size-bufSizeFromCursor,
		buf.info.CursorPosition.ToUint(), &charsWritten)
	if err != nil {
		return os.NewSyscallError("sys.FillConsoleOutputCharacter", err)
	}
	return nil
}

/*// ClearLine clears the line moving the cursor to the beginning of the line.
func (buf *Buffer) ClearLine() error {
	rect := &sys.SMALL_RECT{0, buf.info.Size.Y, buf.info.Window.Right, buf.info.Window.Bottom}
	coord := sys.COORD{0, buf.info.Size.Y}

	err := sys.ScrollConsoleScreenBuffer(syscall.Stdout, rect, nil, coord.ToUint(), buf.charClear)
	if err != nil {
		return os.NewSyscallError("sys.ScrollConsoleScreenBuffer", err)
	}
	return nil
}*/

/*
// Clears to end of line in text window.
// clreol clears all characters from the cursor position to the end of the
// line within the current text window, without moving the cursor.
// Borrar hasta el final de la línea requiere borrar tanto los caracteres
// como los atributos.
void clreol ()
{
   COORD coord;
   DWORD escrito;

   coord.X = vActual.winleft+vActual.curx-1;
   coord.Y = vActual.wintop+vActual.cury-1;
   
   FillConsoleOutputCharacter(GetStdHandle(STD_OUTPUT_HANDLE), ' ',
      vActual.screenwidth - vActual.curx + 1, coord, &escrito);
   FillConsoleOutputAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
      vActual.attribute, vActual.screenwidth - vActual.curx + 1,
      coord, &escrito);
   gotoxy(vActual.curx, vActual.cury);
} 
*/

// Movement

// Goto moves the cursor to the given position.
func Goto(x, y int) error {
	if x < 0 || y < 0 {
		return nil
	}

	coord := sys.COORD{int16(x), int16(y)}
	if err := sys.SetConsoleCursorPosition(syscall.Stdout, coord.ToUint()); err != nil {
		return os.NewSyscallError("sys.SetConsoleCursorPosition", err)
	}
	return nil
}

/*
if(x < 1 || x > vActual.screenwidth ||
      y < 1 || y > vActual.screenheight) return;
   vActual.curx = x;
   vActual.cury = y;
   c.X = vActual.winleft + x - 1;
   c.Y = vActual.wintop + y - 1;
   SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c); 
*/

func (b *Buffer) GotoUp() error {
	coord := sys.COORD{int16(b.info.CursorPosition.X), int16(b.info.CursorPosition.Y-1)}
	if err := sys.SetConsoleCursorPosition(syscall.Stdout, coord.ToUint()); err != nil {
		return os.NewSyscallError("sys.SetConsoleCursorPosition", err)
	}
	return nil
}

func (b *Buffer) GotoDown() error {
	coord := sys.COORD{int16(b.info.CursorPosition.X), int16(b.info.CursorPosition.Y+1)}
	if err := sys.SetConsoleCursorPosition(syscall.Stdout, coord.ToUint()); err != nil {
		return os.NewSyscallError("sys.SetConsoleCursorPosition", err)
	}
	return nil
}

func (b *Buffer) GotoForward() error {
	coord := sys.COORD{int16(b.info.CursorPosition.X+1), int16(b.info.CursorPosition.Y)}
	if err := sys.SetConsoleCursorPosition(syscall.Stdout, coord.ToUint()); err != nil {
		return os.NewSyscallError("sys.SetConsoleCursorPosition", err)
	}
	return nil
}

func (b *Buffer) GotoBackward() error {
	coord := sys.COORD{int16(b.info.CursorPosition.X-1), int16(b.info.CursorPosition.Y)}
	if err := sys.SetConsoleCursorPosition(syscall.Stdout, coord.ToUint()); err != nil {
		return os.NewSyscallError("sys.SetConsoleCursorPosition", err)
	}
	return nil
}
